#!/bin/bash -l
hostname
echo "Deploying app"
pwd
ls -l
export PATH="$PATH:$HOME/.rvm/bin"
source $HOME/.rvm/scripts/rvm
export DOMAIN_NAME=test
export RAILS_ENV=test
#bundle install
#rails generate
#brakeman -f tabs -o brakeman-output
#rm -rf /home/jenkins/release/
#unzip -d /home/jenkins/release/ app.zip 
#cd /home/jenkins/release/
#PID_PUMA=`ps -ef|grep puma |grep -v grep|awk '{print $2}'`
for PID_PUMA in $(ps -ef|grep puma |grep -v grep|awk '{print $2}')
do
	kill ${PID_PUMA}
done
#if [[ ! -z "$PID_PUMA" ]] then
#	kill $PID_PUMA
#fi
rvm use 2.5.1
JENKINS_NODE_COOKIE=released rails server --daemon
#ps -ef |grep puma
#sleep 10
