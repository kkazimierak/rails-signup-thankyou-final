#!/bin/bash -l
echo "Deploying app"
hostname
echo "Deploying app"
pwd
ls -l
echo "Loading rvm"
export PATH="$PATH:$HOME/.rvm/bin"
source $HOME/.rvm/scripts/rvm
export DOMAIN_NAME=test
export RAILS_ENV=test
rvm use 2.5.1
bundle install
#rails generate
brakeman -q -f json -o brakeman-output.json --no-progress --no-exit-on-warn
#rails server --daemon
tar -zcvf ../app.tar.gz *
mv ../app.tar.gz .
